<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="style.css">
  

</head>

<body>
  <!--form-->
  <fieldset style="width:fit-content; margin:170px; auto  10px; font-size: 30px; justify-content:center;">
    <form action="./site2.php" method="POST">
      <legend>Product add</legend>
      <label>SKU:<br />
        <input type="text" name="SKU" pattern="[0-9.]+" required id="SKU"></label><br />
      <label>Name:<br />
        <input type="text" name="name" required id="name" /> </label><br>
      <label>Price($):<br />
        <input required id="price" pattern="[0-9.]+" type="text" name="price"></label><br />

      <!--switch/form -->
      <select id="type" onchange="admSelectCheck(this);">
        <option id="admOption" value="0">DVD</option>
        <option id="admOption" value="0">Book</option>
        <option id="admOption" value="0">Furniture</option>
      </select>
      <div id="admDivCheck" style="display:none;">
        <label for="formGroupExampleInput" class="form-label">Size/Weight/Dimension</label>
        <input type="text" class="form-control" required id="size" name="size" placeholder="Please fill in">
      </div>

      <!--button-->
      <input type="Submit" name="Submit" value="Save">
      <input type="reset" value="Cancel">

    </form>
  </fieldset>

  <!--switch form js-->
  <script>
    function admSelectCheck(nameSelect) {
      console.log(nameSelect);
      if (nameSelect) {
        admOptionValue = document.getElementById("admOption").value;

        if (admOptionValue == nameSelect.value) {
          document.getElementById("admDivCheck").style.display = "block";
        } else {
          document.getElementById("admDivCheck").style.display = "none";
        }
      } else {
        document.getElementById("admDivCheck").style.display = "none";
      }
    }
  </script>
</body>

</html>